package marktwaage;

import java.util.ArrayList;

public class bausteine {
	private ArrayList<Long> gewichte1;
	private ArrayList<Long> gewichte2;
	private long abzubildendeZahl;
	private long diffZuAbzubildendeZahl;

	public bausteine(ArrayList<Long> gewichte1, ArrayList<Long> gewichte2, long abzubildendeZahl,long diffZuAbzubildendeZahl) {
		this.gewichte1 = gewichte1;
		this.gewichte2 = gewichte2;
		this.abzubildendeZahl = abzubildendeZahl;
		this.diffZuAbzubildendeZahl=diffZuAbzubildendeZahl;
	}

	public ArrayList<Long> getGewichte1() {
		return gewichte1;
	}

	public void setGewichte1(ArrayList<Long> gewichte1) {
		this.gewichte1 = gewichte1;
	}

	public ArrayList<Long> getGewichte2() {
		return gewichte2;
	}

	public void setGewichte2(ArrayList<Long> gewichte2) {
		this.gewichte2 = gewichte2;
	}

	public long getAbzubildendeZahl() {
		return abzubildendeZahl;
	}

	public void setAbzubildendeZahl(long abzubildendeZahl) {
		this.abzubildendeZahl = abzubildendeZahl;
	}

	public long getDiffZuAbzubildendeZahl() {
		return diffZuAbzubildendeZahl;
	}

	public void setDiffZuAbzubildendeZahl(long diffZuAbzubildendeZahl) {
		this.diffZuAbzubildendeZahl = diffZuAbzubildendeZahl;
	}
}
