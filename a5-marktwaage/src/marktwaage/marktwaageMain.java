package marktwaage;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class marktwaageMain {
	public static HashMap<Long, ArrayList<ArrayList<Long>>> hmap = new HashMap<Long, ArrayList<ArrayList<Long>>>();
	public static ArrayList<Long> arr = einlesen(wahlDerDatei());
	public static ArrayList<ArrayList<Long>> soMoeglich = new ArrayList<ArrayList<Long>>();
	public static Long keyPublic;
	public static bausteine Baustein;
	public static long gesucht;
	public static int i;
	
	public static void main(String[] args) {
		
		Instant zeit1 = Instant.now();

		boolean boolIstMoeglich;
		ArrayList<Long> arrListSmall = new ArrayList<Long>(arr);

		createHashMap(arr);
		System.out.println();
		// zahlen = null;
		//System.out.println(hmap.keySet());
		//System.out.println(hmap.size());
		//System.out.println("done-done");
		
		//System.out.println(hmap);
		
		for (gesucht = 10; gesucht <= 10000; gesucht += 10) {
			Baustein = null;
			soMoeglich.clear();
			boolIstMoeglich = false;

			if (hmap.containsKey(gesucht)) {
				System.out.println(gesucht + " true    " + new ArrayList<Long>() + " " + hmap.get(gesucht).get(0));
			} else {
				for (Long key : hmap.keySet()) {
					keyPublic = key;
					long rest = Math.abs(gesucht - key);
					for (i = 0; i < hmap.get(key).size(); i++) {
						arrListSmall = removeArray(arr, hmap.get(key).get(i));
						boolIstMoeglich = doesContainExtended(arrListSmall, rest);
						if (boolIstMoeglich)
							break;
					}
					if (boolIstMoeglich)
						break;
				}

				if (boolIstMoeglich) {
					ArrayList<ArrayList<Long>> ARARA = simplify(soMoeglich.get(0), hmap.get(keyPublic).get(i));
					System.out.println(gesucht + " true    " + ARARA.get(0) + " " + ARARA.get(1));
					//System.out.println(hasWholeArr(giveList(arr), addAnArrayToAnother(ARARA.get(0), ARARA.get(1))));
				} else {
					for (Long tttKey : hmap.keySet()) {
						if (Math.abs(tttKey - gesucht) < Baustein.getDiffZuAbzubildendeZahl()) {
							Baustein = new bausteine(hmap.get(tttKey).get(0), new ArrayList<Long>(), gesucht,
									Math.abs(tttKey - gesucht));
						}
					}
					System.out.print(gesucht + " false");
					System.out.println("   " + Baustein.getGewichte2() + " " + Baustein.getGewichte1() + " "
							+ Baustein.getDiffZuAbzubildendeZahl());
					//System.out.println(hasWholeArr(giveList(arr),
						//	addAnArrayToAnother(Baustein.getGewichte2(), Baustein.getGewichte1())));
				}
			}
		}
		System.out.println(
				"Ausgegebenes Format: \n[gewicht] [true] [gewichte für links] [gewichte für rechts]\n[gewicht] [false] [gewichte für links] [gewichte für rechts] [so weit entfernt von [gewicht]]");
		Instant zeit2 = Instant.now();
		System.out.println();
		calculateTimeDiff(zeit1, zeit2);
	}

	public static boolean doesContainExtended(ArrayList<Long> gewichte, long target) {
		if (gewichte.contains(target)) {
			soMoeglich.add(new ArrayList<Long>((int) target));
			return true;
		}
		if (!hmap.containsKey(target)) {
			addBaustein(target, nearestKeyPlus(hmap, target));
			addBaustein(target, nearestKeyMinus(hmap, target));
			return false;
		} else {
			// System.out.println(target);
			for (int i = 0; i < hmap.get(target).size(); i++) {
				if (hasWholeArr(removeArray(gewichte, hmap.get(keyPublic).get(0)), hmap.get(target).get(i))) {
					soMoeglich.add(hmap.get(target).get(i));
					ArrayList<Long> links = new ArrayList<Long>();
					links.add(keyPublic);
					return true;
				}
			}
			addBaustein(target, nearestKeyPlus(hmap, target));
			addBaustein(target, nearestKeyMinus(hmap, target));
		}
		return false;
	}

	public static void addBaustein(long target, long nearestKey) {
		ArrayList<ArrayList<Long>> ARARARA = simplify(hmap.get(nearestKey).get(0), hmap.get(keyPublic).get(0));
		ArrayList<Long> nearestKeyArr = ARARARA.get(0);
		ArrayList<Long> links = ARARARA.get(1);

		long intDiff = getIntDifferenceBetweenArrays(links, nearestKeyArr, gesucht);

		if (!(Baustein == null)) {
			if (!(Baustein == null) && Baustein.getDiffZuAbzubildendeZahl() > intDiff) {
				Baustein = new bausteine(links, nearestKeyArr, gesucht, intDiff);
			}
		} else {
			Baustein = new bausteine(links, nearestKeyArr, gesucht, intDiff);
		}
	}

	public static ArrayList<Long> addAnArrayToAnother(ArrayList<Long> arr1, ArrayList<Long> arr2) {
		for (int i = 0; i < arr2.size(); i++) {
			arr1.add(arr2.get(i));
		}
		return arr1;

	}

	public static Long nearestKeyMinus(HashMap<Long, ArrayList<ArrayList<Long>>> map, Long target) {
		double minDiff = Double.MAX_VALUE;
		Long nearest = null;
		for (Long key : map.keySet()) {
			double diff = Math.abs((double) target - (double) key);
			if (diff < minDiff) {
				nearest = key;
				minDiff = diff;
			}
		}
		return nearest;
	}

	public static Long nearestKeyPlus(HashMap<Long, ArrayList<ArrayList<Long>>> map, Long target) {
		double minDiff = Double.MAX_VALUE;
		Long nearest = null;
		for (Long key : map.keySet()) {
			double diff = Math.abs((double) target - (double) key);
			if (diff <= minDiff) {
				nearest = key;
				minDiff = diff;
			}
		}
		return nearest;
	}

	public static boolean hasWholeArr(ArrayList<Long> containsArr, ArrayList<Long> arr) {
		for (int i = 0; i < arr.size(); i++) {
			if (!containsArr.contains(arr.get(i))) {
				return false;
			}
			containsArr.remove(arr.get(i));
		}
		return true;
	}

	public static long getIntDifferenceBetweenArrays(ArrayList<Long> links, ArrayList<Long> partial,
			Long gesucht) {
		int a1 = 0;
		int a2 = 0;
		for (int i = 0; i < links.size(); i++)
			a1 += links.get(i);
		for (int i = 0; i < partial.size(); i++)
			a2 += partial.get(i);
		return Math.abs(Math.abs(a1 - a2) - gesucht);
	}

	public static void createHashMap(ArrayList<Long> arr) {
		for (int i = 0; i < arr.size(); i++)
			giveAllPossibleNumbers(arr, 0, i, 0);
		//System.out.println("mid-done");
		for (Long KEY : hmap.keySet()) 
			giveAllWaysToCreateANumber(arr, KEY, new ArrayList<Long>());

	}

	public static void giveAllPossibleNumbers(ArrayList<Long> arr, long start, long deep, long sum) {
		if (deep == 0) {
			for (int i = (int) start; i < arr.size(); i++)
				if (!hmap.containsKey(sum + arr.get(i)))
					hmap.put(sum + arr.get(i), null);
		} else {
			for (int i = (int) start; i < arr.size(); i++)
				giveAllPossibleNumbers(arr, i + 1, deep - 1, sum + arr.get(i));
		}
	}

	public static void giveAllWaysToCreateANumber(ArrayList<Long> zahlen, long gesucht, ArrayList<Long> restArr) {
		int currSum = 0;
		for (long x : restArr)
			currSum += x;
		if (currSum == gesucht) {
			if (hmap.get(gesucht) == null) {
				ArrayList<ArrayList<Long>> putter = new ArrayList<ArrayList<Long>>();
				putter.add(restArr);
				hmap.put(gesucht, putter);
			} else {
				if (!hmap.get(gesucht).contains(restArr)) {
					ArrayList<ArrayList<Long>> putter = new ArrayList<ArrayList<Long>>(hmap.get(gesucht));
					putter.add(restArr);
					hmap.put(gesucht, putter);
				}
			}
		}
		if (currSum >= gesucht)
			return;
		for (int i = 0; i < zahlen.size(); i++) {
			ArrayList<Long> remaining = new ArrayList<Long>();
			long zahlAnI = zahlen.get(i);
			for (int j = i + 1; j < zahlen.size(); j++)
				remaining.add(zahlen.get(j));
			ArrayList<Long> restArr1 = new ArrayList<Long>(restArr);
			restArr1.add(zahlAnI);
			giveAllWaysToCreateANumber(remaining, gesucht, restArr1);
		}
	}

	public static ArrayList<Long> einlesen(String dateiName) {
		ArrayList<Long> gewichte = new ArrayList<Long>();
		try {
			File myFile = new File(dateiName);
			Scanner myReader = new Scanner(myFile);

			myReader.nextInt();

			while (myReader.hasNextInt()) {
				long d = myReader.nextInt();
				long x = myReader.nextInt();
				for (int i = 0; i < x; i++) {
					gewichte.add(d);
				}
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		return gewichte;
	}

	public static ArrayList<Long> giveList(ArrayList<Long> list) {
		ArrayList<Long> returnArray = new ArrayList<Long>();
		for (int i = 0; i < list.size(); i++)
			returnArray.add(list.get(i));
		return returnArray;
	}

	public static ArrayList<Long> removeArray(ArrayList<Long> hauptList, ArrayList<Long> entfernen) {
		hauptList = giveList(arr);
		for (int i = 0; i < entfernen.size(); i++)
			hauptList.remove(hauptList.indexOf(entfernen.get(i)));
		return hauptList;
	}

	public static void calculateTimeDiff(Instant zeit1, Instant zeit2) {
		Duration d1 = Duration.between(zeit1, zeit2);
		System.out.println(d1.getSeconds() + ":" + d1.getNano());
	}

	public static ArrayList<ArrayList<Long>> simplify(ArrayList<Long> arrAdin, ArrayList<Long> arrDwe) {
		ArrayList<Long> arrAdin1 = new ArrayList<>(arrAdin);
		ArrayList<Long> arrDwe1 = new ArrayList<>(arrDwe);
		for (Long i : arrAdin) {
			arrDwe1.remove(i);
		}
		for (Long i : arrDwe) {
			arrAdin1.remove(i);
		}
		ArrayList<ArrayList<Long>> arr = new ArrayList<ArrayList<Long>>();
		arr.add(arrAdin1);
		arr.add(arrDwe1);
		return arr;
	}
	public static String wahlDerDatei() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben sie bite als erstes eine Einzulesende Datei ein, oder die Zahl in den Klammern: ");
		System.out.println("Möglichkeiten: " + " gewichtsstuecke0.txt oder [0]");
		System.out.println("\t\t" + "gewichtsstuecke1.txt oder [1]");
		System.out.println("\t\t" + "gewichtsstuecke2.txt oder [2]");
		System.out.println("\t\t" + "gewichtsstuecke3.txt oder [3]");
		System.out.println("\t\t" + "gewichtsstuecke4.txt oder [4]");
		System.out.println("\t\t" + "gewichtsstuecke5.txt oder [5]");
		System.out.println("\t\tWie in der Dokumentation beschrieben \"gewichtsstuecke5.txt\" funktioniert nicht.");

		String datei = sc.next();
		sc.close();
		switch (datei) {
		case "0":
			datei = "gewichtsstuecke0.txt";
			break;
		case "1":
			datei = "gewichtsstuecke1.txt";
			break;
		case "2":
			datei = "gewichtsstuecke2.txt";
			break;
		case "3":
			datei = "gewichtsstuecke3.txt";
			break;
		case "4":
			datei = "gewichtsstuecke4.txt";
			break;
		case "5":
			datei = "gewichtsstuecke5.txt";
			break;

		default:
			System.out.println("Falsche eingabe, bitte Programm neu starten");
			System.exit(0);
			break;
		}
		return datei;
	}
}
