
public class figur {
	private String farbe;

	public figur(String farbe) {
		this.farbe = farbe;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public void print() {
			System.out.print(this.farbe);
	}
}
