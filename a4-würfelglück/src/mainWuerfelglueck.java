import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class mainWuerfelglueck {
	public static Random ran = new Random();
	public static ArrayList<int[]> hauptWuerfel = einlesen("wuerfel0.txt");
	public static ArrayList<figur> figurenSchwarzW1 = new ArrayList<figur>();
	public static ArrayList<figur> figurenGruenW2 = new ArrayList<figur>();

	public static figur[] spielfeldSchwarzW1 = new figur[40];
	public static figur[] spielfeldGruenW2 = new figur[40];

	public static figur[] zielGruen = new figur[4];
	public static figur[] zielSchwarz = new figur[4];

	public static final int umrK = 20;
	public static int gefBei = -2;

	public static int[] rangliste = new int[hauptWuerfel.size()];

	public static ArrayList<int[][]> allComb = new ArrayList<int[][]>();

	public static int h = 0;

	public static void main(String[] args) throws Exception {
		print(hauptWuerfel);
		// to-do
		// bei 6 gewürfelt
		getAllComb(hauptWuerfel, new int[2][], 2, 0);

		/*for (int j = 0; j < allComb.size(); j++) {
			for (int i = 0; i < 10; i++) {
				start(allComb.get(j)[0], allComb.get(j)[1]);
				figurenSchwarzW1 = new ArrayList<figur>();
				figurenGruenW2 = new ArrayList<figur>();
				spielfeldSchwarzW1 = new figur[40];
				spielfeldGruenW2 = new figur[40];
				zielGruen = new figur[4];
				zielSchwarz = new figur[4];
				gefBei = -2;
			}
		}*/

		System.out.println();
		int j = 1;
		print(allComb.get(j));
		start(allComb.get(j)[0], allComb.get(j)[1]);

		System.out.println();
		print(rangliste);
	}

	public static void start(int[] wuerfel1, int[] wuerfel2) {
		for (int i = 0; i < 4; i++) {
			figurenSchwarzW1.add(new figur("schwarz"));
			figurenGruenW2.add(new figur("gruen"));
		}

		boolean binZuFaul = true;
		while (binZuFaul) {
			int gewWuerfel1 = wuerfeln(wuerfel1);
			int gewWuerfel2 = wuerfeln(wuerfel1);
			if (gewWuerfel1 > gewWuerfel2) {
				spiele(wuerfel1, wuerfel2, figurenSchwarzW1, spielfeldSchwarzW1, zielSchwarz, figurenGruenW2,
						spielfeldGruenW2, zielGruen);
				binZuFaul = false;
			} else if (gewWuerfel2 > gewWuerfel1) {
				spiele(wuerfel2, wuerfel1, figurenGruenW2, spielfeldGruenW2, zielGruen, figurenSchwarzW1,
						spielfeldSchwarzW1, zielSchwarz);
				binZuFaul = false;
			}
		}

	}

	public static void spiele(int[] wuerfel1, int[] wuerfel2, ArrayList<figur> figurArrBeginner,
			figur[] spielfeldBeginner, figur[] zielBeginner, ArrayList<figur> figurArrLetzter, figur[] spielfeldLetzter,
			figur[] zielLetzter) {

		if (!contains(wuerfel1, 6)) {
			addToRangliste(wuerfel2);
			return;
		} else if (!contains(wuerfel2, 6)) {
			addToRangliste(wuerfel1);
			return;
		}
		int g = 0;
		while (!isFull(zielBeginner) || !isFull(zielLetzter)) {
			int wurf = wuerfeln(wuerfel1);
			boolean check1 = true;
			ziehe(wuerfel1, wurf, figurArrBeginner, zielBeginner, spielfeldBeginner, spielfeldLetzter, check1);
			gefBei = -2;
			if (isFull(zielBeginner)) {
				addToRangliste(wuerfel1);
				break;
			}

			int wurf2 = wuerfeln(wuerfel2);
			boolean check2 = true;
			ziehe(wuerfel2, wurf2, figurArrLetzter, zielLetzter, spielfeldLetzter, spielfeldBeginner, check2);
			gefBei = -2;
			if (isFull(zielLetzter)) {
				addToRangliste(wuerfel2);
				break;
			}
			/*
			 * g++; if (g > 200) { g = 0; h++; if (h > 2000) { h=0; return; } reset();
			 * //System.gc(); start(wuerfel1, wuerfel2); }
			 */
		}
	}

	public static void ziehe(int[] wuerfel, int wurf, ArrayList<figur> arrListFigur, figur[] zielArr, figur[] spielfeld,
			figur[] spielfeldGegner, boolean checkZiel) {

		if (checkZiel) {
			if (figurArrIsEmpty(spielfeld) && figurArrEverythingInBack(zielArr) && wurf == 6) {
				if (!arrListFigur.isEmpty()) {
					if (spielfeld[wurf - 1] == null) {
						spielfeld[wurf - 1] = arrListFigur.get(0);
						arrListFigur.remove(0);
						return;
					}
				} else {
					return;
				}

			} else if (figurArrIsEmpty(spielfeld) && figurArrEverythingInBack(zielArr) && wurf != 6) {
				return;
			}
			if (getFirstUsableInZielArr(zielArr, gefBei) != -1) {
				if (getFirstUsableInZielArr(zielArr, gefBei) + wurf >= zielArr.length) {
					gefBei = getFirstUsableInZielArr(zielArr, gefBei);
					ziehe(wuerfel, wurf, arrListFigur, zielArr, spielfeld, spielfeldGegner, checkZiel);
					return;
				}
				if (zielArr[getFirstUsableInZielArr(zielArr, gefBei) + wurf] != null) {
					gefBei = getFirstUsableInZielArr(zielArr, gefBei);
					ziehe(wuerfel, wurf, arrListFigur, zielArr, spielfeld, spielfeldGegner, checkZiel);
					return;
				}
				if (zielArr[getFirstUsableInZielArr(zielArr, gefBei) + wurf] == null) {
					int d = getFirstUsableInZielArr(zielArr, gefBei);
					zielArr[d + wurf] = zielArr[d];
					zielArr[d] = null;
					if (wurf == 6) {
						int ffff = wuerfeln(wuerfel);
						gefBei = -2;
						ziehe(wuerfel, ffff, arrListFigur, zielArr, spielfeld, spielfeldGegner, true);
					}
					return;
				}
			} else {
				checkZiel = false;
				gefBei = -2;
			}
		}
		if (getIndexOfFirstInSpielfeld(spielfeld, gefBei) != -1) {
			if (!((getIndexOfFirstInSpielfeld(spielfeld, gefBei) + wurf) > 39)) {
				if (spielfeld[getIndexOfFirstInSpielfeld(spielfeld, gefBei) + wurf] == null) {
					if (getIndexOfFirstInSpielfeld(spielfeld, gefBei) - umrK >= 0) {
						if (spielfeldGegner[getIndexOfFirstInSpielfeld(spielfeld, gefBei) - umrK] != null) {
							gefBei = getIndexOfFirstInSpielfeld(spielfeld, gefBei);
							checkZiel = false;
							ziehe(wuerfel, wurf, arrListFigur, zielArr, spielfeld, spielfeldGegner, checkZiel);
							return;
						} else {
							int d = getIndexOfFirstInSpielfeld(spielfeld, gefBei);
							spielfeld[d + wurf] = spielfeld[d];
							spielfeld[d] = null;
							if (wurf == 6) {
								int ffff = wuerfeln(wuerfel);
								gefBei = -2;
								ziehe(wuerfel, ffff, arrListFigur, zielArr, spielfeld, spielfeldGegner, true);
							}
							return;
						}
					}
				}
				int d = getIndexOfFirstInSpielfeld(spielfeld, gefBei);
				spielfeld[d + wurf] = spielfeld[d];
				spielfeld[d] = null;
				if (wurf == 6) {
					int ffff = wuerfeln(wuerfel);
					gefBei = -2;
					ziehe(wuerfel, ffff, arrListFigur, zielArr, spielfeld, spielfeldGegner, true);
				}
				return;
			} else if (getIndexOfFirstInSpielfeld(spielfeld, gefBei) + wurf >= spielfeld.length + zielArr.length) {
				gefBei = getIndexOfFirstInSpielfeld(spielfeld, gefBei);
				checkZiel = false;
				ziehe(wuerfel, wurf, arrListFigur, zielArr, spielfeld, spielfeldGegner, checkZiel);
				return;
			} else {
				if (getIndexOfFirstInSpielfeld(spielfeld, gefBei) != -1) {
					int d = getIndexOfFirstInSpielfeld(spielfeld, gefBei);
					if (zielArr[d + wurf - spielfeld.length] == null) {
						zielArr[d + wurf - spielfeld.length] = spielfeld[d];
						spielfeld[d] = null;

						if (wurf == 6) {
							int ffff = wuerfeln(wuerfel);
							gefBei = -2;
							ziehe(wuerfel, ffff, arrListFigur, zielArr, spielfeld, spielfeldGegner, true);
						}

					} else {
						gefBei = d;
						checkZiel = false;
						ziehe(wuerfel, wurf, arrListFigur, zielArr, spielfeld, spielfeldGegner, checkZiel);
						return;
					}
					return;
				} else {
					return;
				}
			}
		} else {
			if (wurf == 6 && spielfeld[5] == null && arrListFigur.size() > 0) {
				spielfeld[wurf - 1] = arrListFigur.get(0);
				arrListFigur.remove(0);
				return;
			}
		}

	}

	public static int getIndexOfFirstInSpielfeld(figur[] spielfeld, int letzterGefundenBei) {
		if (letzterGefundenBei == -2) {
			letzterGefundenBei = spielfeld.length;
		}
		for (int i = letzterGefundenBei - 1; i >= 0; i--)
			if (spielfeld[i] != null)
				return i;
		return -1;
	}

	public static boolean figurArrIsEmpty(figur[] arr) {
		for (int i = 0; i < arr.length; i++)
			if (arr[i] != null)
				return false;
		return true;
	}

	public static boolean figurArrEverythingInBack(figur[] arr) {
		int za = 0;
		for (int i = 0; i < arr.length; i++)
			if (arr[i] != null)
				za++;

		int co = 0;
		for (int i = arr.length - 1; i >= 0; i--) {
			if (arr[i] != null) {
				co++;
			} else {
				if (co == za)
					return true;
				else
					return false;
			}

		}
		if (co == za)
			return true;
		return false;
	}

	public static int wuerfeln(int[] arr) {
		return arr[ran.nextInt(arr.length)];
	}

	public static void print(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++)
			System.out.print(arr[i] + ",");
		System.out.print(arr[arr.length - 1]);
		System.out.println();
	}

	public static void print(int[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.print("   ");
		}
	}

	public static void print(figur[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			if (arr[i] != null) {
				arr[i].print();
				System.out.print(",");
			} else
				System.out.print("[" + i + "],");
		}
		if (arr[arr.length - 1] != null)
			arr[arr.length - 1].print();
		else
			System.out.print("[" + (arr.length - 1) + "]");
	}

	public static void print(ArrayList<int[]> arr) {
		for (int i = 0; i < arr.size(); i++) {
			print(arr.get(i));
		}
	}

	public static ArrayList<int[]> einlesen(String datName) {
		ArrayList<int[]> arr = new ArrayList<int[]>();
		try {
			File myFile = new File(datName);
			Scanner input = new Scanner(myFile);
			input.nextLine();
			while (input.hasNextLine() && input.hasNextInt()) {
				int[] arr1 = new int[input.nextInt()];
				for (int i = 0; i < arr1.length; i++)
					arr1[i] = input.nextInt();
				arr.add(arr1);
			}
			input.close();
		} catch (FileNotFoundException e) {
			System.out.println("Ayy jai jai, das sieht ja garnicht gut aus (Ein Fehler ist aufgetreten)");
			e.printStackTrace();
		}

		return arr;
	}

	public static int getFirstUsableInZielArr(figur[] ZielArr, int letzterGefundenBei) {
		if (letzterGefundenBei == -2)
			letzterGefundenBei = ZielArr.length;
		int za = 0;
		for (int i = letzterGefundenBei - 1; i >= 0; i--)
			if (ZielArr[i] != null)
				za++;
			else
				break;

		for (int i = letzterGefundenBei - 1 - za; i >= 0; i--)
			if (ZielArr[i] != null)
				return i;

		return -1;
	}

	public static boolean isFull(figur[] arr) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != null) {

			} else {
				return false;
			}
		}
		return true;
	}

	public static void getAllComb(ArrayList<int[]> arr, int[][] erg, int len, int sPos) {
		if (len == 0) {
			allComb.add(erg.clone());
			return;
		}
		for (int i = sPos; i <= arr.size() - len; i++) {
			erg[erg.length - len] = arr.get(i);
			getAllComb(arr, erg, len - 1, i + 1);
		}
	}

	public static void addToRangliste(int[] wuerfel) {
		int t = hauptWuerfel.indexOf(wuerfel);
		rangliste[t] = rangliste[t] + 1;

	}

	public static boolean contains(int[] arr, int ges) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == ges) {
				return true;
			}
		}
		return false;
	}

	public static void reset() {
		figurenSchwarzW1 = new ArrayList<figur>();
		figurenGruenW2 = new ArrayList<figur>();
		spielfeldSchwarzW1 = new figur[40];
		spielfeldGruenW2 = new figur[40];
		zielGruen = new figur[4];
		zielSchwarz = new figur[4];
		gefBei = -2;
	}
}
