package wortsuche;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class wortsucheMethoden {
	public static ArrayList<koordinateCharKlasse> besetzteKoor = new ArrayList<koordinateCharKlasse>();

	public static String wahlDerDatei() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben sie bite als erstes eine Einzulesende Datei ein, oder die Zahl in den Klammern: ");
		System.out.println("Möglichkeiten: " + " worte0.txt oder [0]");
		System.out.println("\t\t" + "worte1.txt oder [1]");
		System.out.println("\t\t" + "worte2.txt oder [2]");
		System.out.println("\t\t" + "worte3.txt oder [3]");
		System.out.println("\t\t" + "worte4.txt oder [4]");
		System.out.println("\t\t" + "worte5.txt oder [5]");

		String datei = sc.next();
		sc.close();
		switch (datei) {
		case "0":
			datei = "worte0.txt";
			break;
		case "1":
			datei = "worte1.txt";
			break;
		case "2":
			datei = "worte2.txt";
			break;
		case "3":
			datei = "worte3.txt";
			break;
		case "4":
			datei = "worte4.txt";
			break;
		case "5":
			datei = "worte5.txt";
			break;

		default:
			System.out.println("Falsche eingabe, bitte Programm neu starten");
			System.exit(0);
			break;
		}
		return datei;
	}

	public static void create(String einlesendeDateiName, boolean createFile, String erstellendeDatName) {
		ArrayList<Object> werteAusDatei = wortsucheMethoden.einlesen(einlesendeDateiName);

		int laengeX = (Integer) werteAusDatei.get(0);
		int laengeY = (Integer) werteAusDatei.get(1);
		@SuppressWarnings("unchecked") // warnings sind egal (gehen mir am Arsch vorbei)
		ArrayList<String> woerter = (ArrayList<String>) werteAusDatei.get(2);

		wortsucheMethoden.createAll3(laengeX, laengeY, woerter, createFile, erstellendeDatName);

	}

	public static ArrayList<Object> einlesen(String dateiName) {// datei einlesen
		int laengeX = 0;
		int laengeY = 0;
		ArrayList<String> woerter = new ArrayList<String>();

		try {
			File myFile = new File(dateiName);
			Scanner input = new Scanner(myFile);
			laengeX = input.nextInt();
			laengeY = input.nextInt();
			input.nextInt();
			input.nextLine();
			while (input.hasNextLine()) {
				woerter.add(input.nextLine());
			}
			input.close();
		} catch (FileNotFoundException e) {
			System.out.println("Ayy jai jai, das sieht ja garnicht gut aus (Ein Fehler ist aufgetreten)");
			e.printStackTrace();
		}

		ArrayList<Object> returnArr = new ArrayList<Object>();
		returnArr.add(laengeX);
		returnArr.add(laengeY);
		returnArr.add(woerter);

		return returnArr;
	}

	public static void createAll3(int laengeX, int laengeY, ArrayList<String> woerter, boolean createFile,
			String datName) {
		if (createFile) {// datei wird kreiert falls true
			ArrayList<char[][]> allArr = new ArrayList<char[][]>();
			allArr.add(wortsucheMethoden.schw1(laengeX, laengeY, woerter));
			allArr.add(wortsucheMethoden.schw2(laengeX, laengeY, woerter));
			allArr.add(wortsucheMethoden.schw3(laengeX, laengeY, woerter));
			try {
				FileWriter myWriter = new FileWriter(datName + ".txt");
				myWriter.write("Wortsuche:\n2D Arrays mit " + laengeX + " und " + laengeY + "\n");
				for (int i = 0; i < allArr.size(); i++) {
					myWriter.write("schw" + (i + 1) + "\n");
					for (int n = 0; n < allArr.get(i).length; n++) {
						for (int j = 0; j < allArr.get(i)[n].length; j++) {
							myWriter.write(allArr.get(i)[n][j] + " ");
						}
						myWriter.write("\n");
					}
					myWriter.write("\n");
				}
				myWriter.close();
				System.out.println("Successfully wrote to the file.");
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		} else {
			wortsucheMethoden.print2DArray(wortsucheMethoden.schw1(laengeX, laengeY, woerter));
			System.out.println();
			wortsucheMethoden.print2DArray(wortsucheMethoden.schw2(laengeX, laengeY, woerter));
			System.out.println();
			wortsucheMethoden.print2DArray(wortsucheMethoden.schw3(laengeX, laengeY, woerter));
		}
	}

	public static char[][] schw1(int laengeX, int laengeY, ArrayList<String> woerter) {// array mit schw1 wird erstellt
		if (laengeX < 0 || laengeY < 0) {
			char[][] duMinusLoser = new char[0][0];
			return duMinusLoser;
		}

		Random ran = new Random();

		char[][] haupt = new char[laengeX][laengeY];
		populate2DArraySchw1(haupt);
		int d = 0;
		for (int i = 0; i < woerter.size(); i++) {// schleife um wörter ins array zu machen
			int x = ran.nextInt(laengeX);

			int y = ran.nextInt(laengeY - woerter.get(i).length() + 1);

			if (checkFillHorizontal(x, y, haupt, woerter.get(i), besetzteKoor)) {
				fillHorizontal(x, y, haupt, woerter.get(i));
			} else {
				i--;
			}
			d++;
			if (d > 200000) {
				System.out.println("Fehler bei der Erstellung, bitte wiederholen");
				System.exit(0);
			}
		}
		besetzteKoor.clear();
		return haupt;
	}

	public static char[][] schw2(int laengeX, int laengeY, ArrayList<String> woerter) {// array mit schw2 wird erstellt
		if (laengeX < 0 || laengeY < 0) {
			char[][] duMinusLoser = new char[0][0];
			return duMinusLoser;
		}

		Random ran = new Random();

		char[][] haupt = new char[laengeX][laengeY];
		populate2DArraySchw2(haupt);
		haupt = removeAllOcassionalWords(haupt, woerter);

		int d = 0;
		for (int i = 0; i < woerter.size(); i++) {
			int which = ran.nextInt(2);
			if (which == 0) {
				int x = ran.nextInt(laengeX);
				int y = ran.nextInt(laengeY - woerter.get(i).length() + 1);
				if (checkFillHorizontal(x, y, haupt, woerter.get(i), besetzteKoor)) {
					fillHorizontal(x, y, haupt, woerter.get(i));
				} else {
					i--;
				}
			} else {
				int x = ran.nextInt(laengeX - woerter.get(i).length() + 1);
				int y = ran.nextInt(laengeY);
				if (checkFillVertical(x, y, haupt, woerter.get(i), besetzteKoor)) {
					fillVertical(x, y, haupt, woerter.get(i));
				} else {
					i--;
				}
			}
			d++;
			if (d > 200000) {
				System.out.println("Fehler bei der Erstellung, bitte wiederholen");
				System.exit(0);
			}
		}
		besetzteKoor.clear();
		return haupt;
	}

	public static char[][] schw3(int laengeX, int laengeY, ArrayList<String> woerter) {// array mit schw3 wird erstellt
		if (laengeX < 0 || laengeY < 0) {
			char[][] duMinusLoser = new char[0][0];
			return duMinusLoser;
		}

		Random ran = new Random();

		char[][] haupt = new char[laengeX][laengeY];

		populate2DArraySchw3(haupt, woerter);

		haupt = removeAllOcassionalWords(haupt, woerter);
		int d = 0;
		for (int i = 0; i < woerter.size(); i++) {
			int which = ran.nextInt(4);

			switch (which) {
			case 0:
				int x = ran.nextInt(laengeX);
				int y = ran.nextInt(laengeY - woerter.get(i).length() + 1);
				if (checkFillHorizontal(x, y, haupt, woerter.get(i), besetzteKoor)) {
					fillHorizontal(x, y, haupt, woerter.get(i));
				} else {
					i--;
				}
				break;
			case 1:
				int x1 = ran.nextInt(laengeX - woerter.get(i).length() + 1);
				int y1 = ran.nextInt(laengeY);
				if (checkFillVertical(x1, y1, haupt, woerter.get(i), besetzteKoor)) {
					fillVertical(x1, y1, haupt, woerter.get(i));
				} else {
					i--;
				}
				break;
			case 2:
				int x2 = ran.nextInt(laengeX - woerter.get(i).length() + 1) + woerter.get(i).length() - 1;
				int y2 = ran.nextInt(laengeY - woerter.get(i).length() + 1);
				if (checkFillDiagonalUO(x2, y2, haupt, woerter.get(i), besetzteKoor)) {
					fillDiagonalUO(x2, y2, haupt, woerter.get(i));
				} else {
					i--;
				}
				break;
			case 3:
				int x3 = ran.nextInt(laengeX - woerter.get(i).length() + 1);
				int y3 = ran.nextInt(laengeY - woerter.get(i).length() + 1);
				if (checkFillDiagonalOU(x3, y3, haupt, woerter.get(i), besetzteKoor)) {
					fillDiagonalOU(x3, y3, haupt, woerter.get(i));
				} else {
					i--;
				}
				break;

			default:
				i--;
				break;
			}
			d++;
			if (d > 200000) {
				System.out.println("Fehler bei der Erstellung, bitte wiederholen");
				System.exit(0);
			}
		}
		besetzteKoor.clear();
		return haupt;
	}

	public static void print2DArray(char[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void populate2DArraySchw1(char[][] arr) { // array wird mit k befüllt -> schwierigkeit 1
		Random ran = new Random();
		char k = (char) (ran.nextInt(26) + 65);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = k;
			}
		}
	}

	public static void populate2DArraySchw2(char[][] arr) { // array wird mit arr1 befüllt -> schwierigkeit 2
		Random ran = new Random();
		char[] bst = new char[ran.nextInt(3) + 3];
		for (int i = 0; i < bst.length; i++) {
			bst[i] = (char) (ran.nextInt(26) + 65);
		}
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = bst[ran.nextInt(bst.length)];
			}
		}
	}

	public static void populate2DArraySchw3(char[][] arr, ArrayList<String> woerter) { // array wird mit substrings von
																						// "woerter" befüllt ->
																						// schwierigkeit 3
		String subWoerter;

		int y = 0;
		for (int i = 0; i < arr.length; i++) {
			subWoerter = subWoerterErstellen(woerter);
			for (int j = 0; j < arr[i].length; j++) {
				if (y >= subWoerter.length()) {
					y = 0;
					subWoerter = subWoerterErstellen(woerter);
				}
				arr[i][j] = subWoerter.charAt(y);
				y++;
			}
			y = 0;
		}
	}

	public static void fillHorizontal(int x, int y, char[][] arr, String wort) {
		// System.out.println("\n"+x+y);
		for (int i = 0; i < wort.length(); i++) {
			arr[x][y + i] = wort.charAt(i);
			besetzteKoor.add(new koordinateCharKlasse(x, y + i, wort.charAt(i)));
		}
	}

	public static boolean checkFillHorizontal(int x, int y, char[][] arr, String wort,
			ArrayList<koordinateCharKlasse> bK) {
		if (x < 0 || y < 0)
			return false;

		if (x >= arr.length || y + wort.length() > arr[0].length)
			return false;

		if (bK.isEmpty())
			return true;

		ArrayList<koordinateCharKlasse> pBK = new ArrayList<koordinateCharKlasse>();

		for (int i = 0; i < wort.length(); i++)
			pBK.add(new koordinateCharKlasse(x, y + i, wort.charAt(i)));

		if (!xYGleichBuchstabeAnders(pBK, bK))
			return false;

		if (!everyFieldSame(pBK, bK))
			return false;

		return true;
	}

	public static void fillVertical(int x, int y, char[][] arr, String wort) {
		for (int i = 0; i < wort.length(); i++) {
			arr[x + i][y] = wort.charAt(i);
			besetzteKoor.add(new koordinateCharKlasse(x + i, y, wort.charAt(i)));
		}
	}

	public static boolean checkFillVertical(int x, int y, char[][] arr, String wort,
			ArrayList<koordinateCharKlasse> bK) {
		if (x < 0 || y < 0)
			return false;

		if (x + wort.length() > arr.length || y > arr[0].length)
			return false;

		if (bK.isEmpty())
			return true;

		ArrayList<koordinateCharKlasse> pBK = new ArrayList<koordinateCharKlasse>();

		for (int i = 0; i < wort.length(); i++)
			pBK.add(new koordinateCharKlasse(x + i, y, wort.charAt(i)));

		if (!xYGleichBuchstabeAnders(pBK, bK))
			return false;

		if (!everyFieldSame(pBK, bK))
			return false;

		return true;

	}

	public static void fillDiagonalUO(int x, int y, char[][] arr, String wort) {// bef�llt von unten links nach oben
		// rechts mit "wort"
		for (int i = 0; i < wort.length(); i++) {
			arr[x - i][y + i] = wort.charAt(i);
			besetzteKoor.add(new koordinateCharKlasse(x - i, y + i, wort.charAt(i)));
		}
	}

	public static boolean checkFillDiagonalUO(int x, int y, char[][] arr, String wort,
			ArrayList<koordinateCharKlasse> bK) {
		if (x < 0 || y < 0)
			return false;

		if (x - wort.length() - 1 < 0 || y + wort.length() >= arr[0].length)
			return false;

		if (bK.isEmpty())
			return true;

		ArrayList<koordinateCharKlasse> pBK = new ArrayList<koordinateCharKlasse>();

		for (int i = 0; i < wort.length(); i++)
			pBK.add(new koordinateCharKlasse(x - i, y + i, wort.charAt(i)));

		if (!xYGleichBuchstabeAnders(pBK, bK))
			return false;

		if (!everyFieldSame(pBK, bK))
			return false;

		return true;
	}

	public static void fillDiagonalOU(int x, int y, char[][] arr, String wort) {// bef�llt von oben links nach unten
		// rechts mit "wort"
		for (int i = 0; i < wort.length(); i++) {
			arr[x + i][y + i] = wort.charAt(i);
			besetzteKoor.add(new koordinateCharKlasse(x + i, y + i, wort.charAt(i)));
		}
	}

	public static boolean checkFillDiagonalOU(int x, int y, char[][] arr, String wort,
			ArrayList<koordinateCharKlasse> bK) {
		if (x < 0 || y < 0)
			return false;

		if (x + wort.length() - 1 > arr.length || y + wort.length() - 1 > arr[0].length)
			return false;

		if (bK.isEmpty())
			return true;

		ArrayList<koordinateCharKlasse> pBK = new ArrayList<koordinateCharKlasse>();

		for (int i = 0; i < wort.length(); i++)
			pBK.add(new koordinateCharKlasse(x + i, y + i, wort.charAt(i)));

		if (!xYGleichBuchstabeAnders(pBK, bK))
			return false;

		if (!everyFieldSame(pBK, bK))
			return false;

		return true;
	}

	public static String subWoerterErstellen(ArrayList<String> woerter) {
		Random ran = new Random();
		int o;
		int t;
		String subWoerter = "";
		for (int i = ran.nextInt(woerter.size()); i < woerter.size(); i++) {
			int random = ran.nextInt(woerter.size());
			o = ran.nextInt(woerter.get(random).length() - 1) + 1;
			t = ran.nextInt(woerter.get(random).length() - o + 1);
			subWoerter += woerter.get(random).substring(t, t + o);
		} // subWoerter mit substrings von woerter.get(i) erstellt
		return subWoerter;
	}

	public static char[][] removeAllOcassionalWords(char[][] haupt, ArrayList<String> woerter) {// falls bei befüllung
																								// des arrays schon
																								// wörter gebildet
																								// wurden
		while (boolCCHorizontal(haupt, woerter) || boolCCVertical(haupt, woerter)) {
			if (boolCCHorizontal(haupt, woerter)) {
				haupt = checkFehlerHorizontal(haupt, woerter);
			}
			if (boolCCVertical(haupt, woerter)) {
				haupt = checkFehlerVertical(haupt, woerter);
			}
		}
		return haupt;
	}

	public static char[][] checkFehlerHorizontal(char[][] arr, ArrayList<String> woerter) {// falls bei befüllung des
																							// arrays schon wörter
																							// gebildet wurden
		Random ran = new Random();
		StringBuilder rowString;
		for (int k = 0; k < woerter.size(); k++) {
			for (int i = 0; i < arr.length; i++) {
				rowString = new StringBuilder(getArrayLineAsStringHorizontal(arr, i));

				while (!(rowString.indexOf(woerter.get(k)) == -1)) {
					char x = (char) (ran.nextInt(26) + 65);
					arr[i][rowString.indexOf(woerter.get(k)) + ran.nextInt(woerter.get(k).length())] = x;
					rowString.setCharAt(rowString.indexOf(woerter.get(k)), x);

				}
			}
		}

		return arr;
	}

	public static boolean boolCCHorizontal(char[][] arr, ArrayList<String> woerter) {
		StringBuilder rowString;
		for (int k = 0; k < woerter.size(); k++) {
			for (int i = 0; i < arr.length; i++) {
				rowString = new StringBuilder(getArrayLineAsStringHorizontal(arr, i));
				for (int j = 0; j < arr[i].length; j++) {
					if (!(rowString.indexOf(woerter.get(k)) == -1)) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public static char[][] checkFehlerVertical(char[][] arr, ArrayList<String> woerter) {// falls bei befüllung des
																							// arrays schon wörter
																							// gebildet wurden
		Random ran = new Random();
		StringBuilder rowString;
		for (int k = 0; k < woerter.size(); k++) {
			for (int j = 0; j < arr[0].length; j++) {
				rowString = new StringBuilder(getArrayLineAsStringVertical(arr, j));
				while (!(rowString.indexOf(woerter.get(k)) == -1)) {
					char x = (char) (ran.nextInt(26) + 65);
					arr[rowString.indexOf(woerter.get(k)) + ran.nextInt(woerter.get(k).length())][j] = x;
					rowString.setCharAt(rowString.indexOf(woerter.get(k)), x);

				}
			}
		}

		return arr;
	}

	public static boolean boolCCVertical(char[][] arr, ArrayList<String> woerter) {
		StringBuilder rowString;
		for (int k = 0; k < woerter.size(); k++) {
			for (int j = 0; j < arr[0].length; j++) {
				rowString = new StringBuilder(getArrayLineAsStringVertical(arr, j));
				for (int i = 0; i < arr.length; i++) {
					if (!(rowString.indexOf(woerter.get(k)) == -1)) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public static String getArrayLineAsStringHorizontal(char[][] arr, int row) {
		String rowAsString = "";
		for (int i = 0; i < arr[row].length; i++) {
			rowAsString += arr[row][i];
		}

		return rowAsString;
	}

	public static String getArrayLineAsStringVertical(char[][] arr, int column) {
		String columnAsString = "";
		for (int i = 0; i < arr.length; i++) {
			columnAsString += arr[i][column];
		}
		return columnAsString;
	}

	public static boolean xYGleichBuchstabeAnders(ArrayList<koordinateCharKlasse> pBK,
			ArrayList<koordinateCharKlasse> bK) {
		for (int i = 0; i < pBK.size(); i++) {
			for (int j = 0; j < bK.size(); j++) {
				if (pBK.get(i).getxKoor() == bK.get(j).getxKoor() && pBK.get(i).getyKoor() == bK.get(j).getyKoor()
						&& pBK.get(i).getBuchstabe() != bK.get(j).getBuchstabe()) {
					return false;
				}
			}
		}
		return true;
	}

	public static boolean everyFieldSame(ArrayList<koordinateCharKlasse> pBK, ArrayList<koordinateCharKlasse> bK) {
		int c = 0;
		for (int i = 0; i < pBK.size(); i++) {
			for (int j = 0; j < bK.size(); j++) {
				if (pBK.get(i).getxKoor() == bK.get(j).getxKoor() && pBK.get(i).getyKoor() == bK.get(j).getyKoor()
						&& pBK.get(i).getBuchstabe() == bK.get(j).getBuchstabe()) {
					c++;
				}
			}
		}

		if (c == pBK.size())
			return false;
		return true;
	}
}
