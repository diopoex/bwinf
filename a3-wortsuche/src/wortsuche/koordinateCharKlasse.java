package wortsuche;

public class koordinateCharKlasse {
	private int xKoor;
	private int yKoor;
	private char buchstabe;

	public koordinateCharKlasse(int xKoor, int yKoor, char buchstabe) {
		this.xKoor = xKoor;
		this.yKoor = yKoor;
		this.buchstabe = buchstabe;
	}

	public int getxKoor() {
		return xKoor;
	}

	public void setxKoor(int xKoor) {
		this.xKoor = xKoor;
	}

	public int getyKoor() {
		return yKoor;
	}

	public void setyKoor(int yKoor) {
		this.yKoor = yKoor;
	}

	public char getBuchstabe() {
		return buchstabe;
	}

	public void setBuchstabe(char buchstabe) {
		this.buchstabe = buchstabe;
	}
}
