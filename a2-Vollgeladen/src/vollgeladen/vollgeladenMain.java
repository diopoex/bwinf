package vollgeladen;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class vollgeladenMain {

	public static ArrayList<hotelKlasse> hotels = einlesen(wahl());
	public static ArrayList<hotelKlasse> bestHotels = new ArrayList<hotelKlasse>();

	public static int anzHotels = hotels.size();
	public static int streckeInsgesamt;

	public static void main(String[] args) {
		hotAusgeben();
	}

	public static void hotAusgeben() {
		double vGeschw = 60;

		int mDistProTag = (int) vGeschw * 6;

		double tage = (vGeschw * (streckeInsgesamt / 60));
		tage /= mDistProTag;
		tage = Math.ceil(tage);
		int bDistanz = 0;

		hotelKlasse bestesHotel = new hotelKlasse();

		for (int i = 0; i < (int) tage; i++) {
			ArrayList<hotelKlasse> hInRange = new ArrayList<hotelKlasse>();

			for (int j = 0; j < hotels.size(); j++) {
				if (tage == 1)
					hInRange.add(hotels.get(j));
				else if (hotels.get(j).getStreckenkilometer() > bDistanz + (mDistProTag / 3)
						&& hotels.get(j).getStreckenkilometer() <= (bDistanz + mDistProTag)) {
					hInRange.add(hotels.get(j));
				}
			}
			double bestBew = 0.0;
			bestesHotel = new hotelKlasse();
			for (int k = 0; k < hInRange.size(); k++) {
				if (tage == 1) {
					if (hInRange.get(k).getBewertung() >= bestBew) {
						bestBew = hInRange.get(k).getBewertung();
						bestesHotel = hInRange.get(k);

					}
				} else {
					int mind = (int) (anzHotels / tage);
					if (i == 0 && hInRange.get(k).getStreckenkilometer() > mind) {
						if (hInRange.get(k).getBewertung() >= bestBew) {
							bestBew = hInRange.get(k).getBewertung();
							bestesHotel = hInRange.get(k);
						}

					} else if (hInRange.get(k).getBewertung() >= bestBew) {
						bestBew = hInRange.get(k).getBewertung();
						bestesHotel = hInRange.get(k);

					}

				}
			}
			System.out.println(
					(i + 1) + ". Hotel: " + bestesHotel.getStreckenkilometer() + "km, " + bestesHotel.getBewertung());
			bDistanz = bestesHotel.getStreckenkilometer();
		}
		System.out.println("");
		System.out.println("Ziel bei: " + streckeInsgesamt + "km");
	}

	public static ArrayList<hotelKlasse> einlesen(String datName) {
		ArrayList<hotelKlasse> arr = new ArrayList<hotelKlasse>();

		try {
			File myFile = new File(datName);
			Scanner input = new Scanner(myFile);

			input.nextInt();
			input.nextLine();
			vollgeladenMain.streckeInsgesamt = input.nextInt();
			input.nextLine();

			while (input.hasNextLine() && input.hasNextInt()) {
				int streckenkilometer = input.nextInt();
				double bew = Double.valueOf(input.next());
				arr.add(new hotelKlasse(streckenkilometer, bew));
			}
			input.close();
		} catch (FileNotFoundException e) {
			System.out.println("Ayy jai jai, das sieht ja garnicht gut aus (Ein Fehler ist aufgetreten)");
			e.printStackTrace();
		}
		return arr;
	}

	public static String wahl() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben sie bite als erstes eine Einzulesende Datei ein, oder die Zahl in den Klammern: ");
		System.out.println("Möglichkeiten:  " + "hotels1.txt oder [1]");
		System.out.println("\t\t" + "hotels2.txt oder [2]");
		System.out.println("\t\t" + "hotels3.txt oder [3]");
		System.out.println("\t\t" + "hotels4.txt oder [4]");
		System.out.println("\t\t" + "hotels5.txt oder [5]");

		String datei = sc.next();
		sc.close();
		switch (datei) {
		case "1":
			datei = "hotels1.txt";
			break;
		case "2":
			datei = "hotels2.txt";
			break;
		case "3":
			datei = "hotels3.txt";
			break;
		case "4":
			datei = "hotels4.txt";
			break;
		case "5":
			datei = "hotels5.txt";
			break;
		default:
			System.out.println("Falsche eingabe, bitte Programm neu starten");
			System.exit(0);
			break;
		}
		return datei;
	}
}
